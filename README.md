
# packaged custom-accordion

Este es un repositorio hecho para su distribución en `bower`.

## Instalación

### bower

```shell
bower install http://USUARIO@lcm-repositorio-fuentes.igrupobbva/scm/eyph/custom-accordion.git --save
```

El `--save` es optativo pero recomendable. Si se incluye esta opción no es necesario incluir ni los estilos ni el javascript en la página principal. Si no se incluye consultar cómo incluirlos en la siguiente sección.

## Uso

### JavaScript

Añadir a los `<script>` del `index.html`:

```html
    <script src="bower_components/custom-datepicker/custom.datepicker.js"></script>
```
Añadir `customDatepicker` como dependencia al módulo del `core`:

```javascript
angular.module('app.core', [..., 'mainAccordion', ...]);
```

### Html

Este componente está compuesto por dos directivas diferentes `accordion` y `accordionGroup`, que funcionarán de manera conjunta, no por separado. El primero `<accordion>` es simplemente un contenedor, relleno mediante un `transclude` de "N" `<accordion-group>`.

Atributos de configuración del `<accordion>`:

* `close-others`: Atributo que indica el modo de funcionamiento del acordeón. El caso típico va a tener el valor `oneAtATime`, que hará que solo uno de los paneles esté abierto a la vez.
* `data-service`: El interior de los paneles se va a cargar mediante llamadas ajax. Cada acordeón va a tener un servicio "propio" para cargar los datos. El caso que está ahora mismo en el paquete contiene un servicio (`fileDataService`) del proyecto de Confirming. Se irán añadiendo otras llamadas dependiendo del uso que se vaya a dar.

Atributos de configuración del `<accordion-group>`:

* `heading`: Texto que va a aparecer como título del panel.
* `subheading`: Texto que va a aparecer como subtítulo del panel.
* `is-open`: Valor lógico que indica el estado inicial del panel, abierto o cerrado.
* `is-disabled`: Valor lógico que indica si el primero está inhabilitado o no.
* `initializer`: Objeto mediante el que se le pasarán los datos que necesite, por ejemplo, para la llamada ajax si es necesario. En Confirming, por ejemplo, sí que es necesario.
* `accordionMethod`: Atributo por el que pasaremos por referencia un método desde el controlador del padre. Este método será invocado la primera vez que se abra el acordeón y su respuesta (en caso de ser una patición ajax o que este devuelva algo), se expodrá al padre a través del atributo 'accordionData'.
* `accordionData`: Atributo por el que podremos acceder a la respuesta de la invocación del método pasado a través del atributo `accordionMethod`.

```html
<accordion close-others="oneAtATime" [data-service="dataService"]>
  <accordion-group heading="<texto>" subheading="<texto>" is-open="false" is-disabled="status.isFirstDisabled" initializer="<objeto>" accordionMethod="<methodReference>" accordionData="ajaxResponse">
   	Datos del interior del panel 1
  </accordion-group>
  [ <accordion-group heading="<texto>" subheading="<texto>" is-open="false" is-disabled="status.isFirstDisabled" initializer="<objeto>" accordionMethod="<methodReference>" accordionData="ajaxResponse">
   	Datos del interior del panel 2
  </accordion-group> ]
</accordion>
```

Un solo panel cerrado:

![Html Image](readme-images/accordion-cerrado.png)

Una vez se ha abierto, su apariencia es:

![Html Image](readme-images/accordion-abierto.png)

En el caso de que haya varios paneles en el mismo acordeón, su aspecto será:

![Html Image](readme-images/accordion-varios.png)

Con el otro panel abierto:

![Html Image](readme-images/accordion-varios-2.png)

Como se ve en las dos imágenes anteriores, no es posible mantener abiertos varios paneles a la vez, al pinchar para desplegar uno se cerrarían automáticamente todos los demás.

## Consideraciones adicionales

### Dependencias

Las dependencia del componente, incluida en el bower.json, es:

```javascript
    "common-resources": "http://USUARIO@lcm-repositorio-fuentes.igrupobbva/scm/eyph/common-resources.git"
```

Esta dependencia hace que el componente contenga todo el js necesario para su funcionamiento y además hace que no necesite tener ni estilos ni imágenes propias, ya que el paquete `common-resources` contiene todo lo necesario.

### A tener en cuenta

* El contenido del interior de los paneles del acordeón puede ser distinto. Puede contener listados, tablas, texto plano... Por ello, cada vez que se utilice el componente el servicio que se le inyecte va a ser diferente, al igual que el objeto de inicialización que se le pasa a cada uno de los paneles. También cabe la posibilidad de que el propio acordeón obtenga la información a mostrar a través de un método definido externamente a la directiva, devolviendo el valor hacia afuera para que pueda ser usado mediante binding dentro del transclude cuyo html será externo al perteneciente a la directiva.
