(function(angular) {
    /**
     * @ngdoc directive
     * @name directive.mainAccordion
     * @requires ui.bootstrap.collapse
     * @description
     * Componente Acordeón
     */
    angular.module('mainAccordion', ['ui.bootstrap.collapse'])
        .constant('accordionConfig', {
            closeOthers: true
        })
        .directive('accordion', function() {
            return {
                controller: AccordionController,
                controllerAs: 'accordion',
                transclude: true,
                templateUrl: function(element, attrs) {
                    return attrs.templateUrl || '../bower_components/custom-accordion/accordion.html';
                }
            };
        });

    AccordionController.$inject = ['$scope', '$injector', '$attrs', 'accordionConfig'];

    /**
     * @ngdoc method
     * @name AccordionController
     * @methodOf directive.mainAccordion
     * @description
     * Controlador que agrupa los elementos del acordeón
     */
    function AccordionController ($scope, $injector, $attrs, accordionConfig) {
        var ctrl = this;

        if($attrs.fileDataService){
            ctrl.fileDataService = $injector.get($attrs.fileDataService);
        }


        ctrl.groups = [];
        ctrl.closeOthers = function(openGroup) {

            var closeOthers = angular.isDefined($attrs.closeOthers) ? $attrs.closeOthers : accordionConfig.closeOthers;
            if (closeOthers) {
                angular.forEach(ctrl.groups, function(group) {
                    if (group !== openGroup) {
                        group.isOpen = false;
                    }
                });
            }
        };
        ctrl.addGroup = function(groupScope) {
            var that = ctrl;
            ctrl.groups.push(groupScope);
            groupScope.$on('$destroy', function(event) {
                that.removeGroup(groupScope);
            });
        };
        ctrl.removeGroup = function(group) {
            var index = ctrl.groups.indexOf(group);
            if (index !== -1) {
                ctrl.groups.splice(index, 1);
            }
        };
    }
})(window.angular);
