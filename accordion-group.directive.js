/* globals _ENV_ */
(function(angular) {
    //ui.bootstrap.collapse

    /**
     * @ngdoc directive
     * @name directive.mainAccordion.accordionGroup
     * @requires dataservice, configApp
     * @description
     * Componente para las piezas del acordeón
     */
    angular.module('mainAccordion')
        .directive('accordionGroup', accordionGroup);


    function accordionGroup () {
        return {
            require: '^accordion',
            transclude: true,
            replace: true,
            templateUrl: function(element, attrs) {
                return attrs.templateUrl || '../bower_components/custom-accordion/accordion-group.html';
            },
            scope: {
                heading: '@',
                subheading: '@',
                panelClass: '@?',
                isOpen: '=?',
                isDisabled: '=?',
                initializer: '=',
                accordionMethod: '=',
                accordionData: '='
            },
            link: function(scope, element, attrs, accordionCtrl) {
                if(scope.accordionMethod && typeof scope.accordionMethod === 'function'){
                    scope.dataRequest = function() {
                        if(!scope.accordionData.data){
                            scope.isDisabled = true;
                            scope.accordionMethod().then(function(response){
                                scope.accordionData.data = JSON.parse(response);
                                scope.isDisabled = false;
                            })
                        }
                    };
                } else if(accordionCtrl.fileDataService) {
                    var objUrl = scope.initializer.objUrlOrders;
                    scope.dataRequest = function() {
                        console.log('tableRequest - Accordion');
                        var requestData = {
                            numeroPagina: 0,
                            claseOrden:  scope.initializer.claseOrden,
                            idFichero: scope.initializer.idFichero
                        };
                        accordionCtrl.fileDataService.getOrdersData(objUrl, requestData).then(function(response) {
                            scope.data = response;
                        });
                    };
                }

                accordionCtrl.addGroup(scope);
                scope.openClass = attrs.openClass || 'panel-open';
                scope.panelClass = attrs.panelClass || 'panel-default';
                scope.$watch('isOpen', function(value) {
                    element.toggleClass(scope.openClass, !!value);

                    if (scope.dataRequest && scope.data === null && scope.isOpen) {
                        scope.dataRequest();
                    }


                    if (value) {
                        accordionCtrl.closeOthers(scope);
                    }
                });
                scope.data = null;
                scope.toggleOpen = function($event) {
                    if (!scope.isDisabled) {
                        if (!$event || $event.which === 32) {
                            scope.isOpen = !scope.isOpen;
                        }
                    }
                };
            }
        };
    }
})(window.angular);
